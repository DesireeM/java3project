
## Name
Mcdonald Inventory System

## Description
Program that allows employees to track items sold at our Mcdonald store and manage a customer’s reward system i.e coupons.
The information that will be shown to the user will be items in the store, the promo codes, users, loyalty points, etc.
There will be 2 console applications for admins and non-admins so for admins they will be able to edit the inventory such as the price and for the non-admins they will make queries such as outputting what the most popular product is.


## Installation
Download the folder on moodle and then open the folder on VScode, depending if the user running the program is a Admin or a Non-Admin (User) and run the approriate application so AdminApp.java or UserApp.java

## Authors and acknowledgment
Desiree Mariano & Sungeun Kim

## License
School final project

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
