/**
 * @author Sungeun Kim 2042714
 * @version November 2023
 * @description Represents the definition and data for two tables: "Burger" and "Beverage".
 * The "Burger" table contains information about different types of burgers,
 * including name, user status, meat, veggies, sauce, discount, and price.
 * The "Beverage" table stores details about various beverages, such as name,
 * user status, drink size, cold drink indicator, price, and discount.
 */

DROP TABLE Burger;
DROP TABLE Beverage;

CREATE TABLE Burger (
    nameOfProduct VARCHAR2(50),
    userStatus VARCHAR2(10),
    meat VARCHAR2(50),
    veggies VARCHAR2(50),
    sauce VARCHAR2(50),
    discount DECIMAL(10,5),
    price DECIMAL(10,5)
);
INSERT INTO Burger
VALUES('Burger', 'User', 'Chicken', 'Lettuce', 'Mayo', 0.25, 6.0);
INSERT INTO Burger
VALUES('Veggie burger', 'Admin', 'vegetarianBeef', 'Lettuce', 'Ketchup', 0.15, 8.0);
COMMIT;

CREATE TABLE Beverage(
    nameOfProduct VARCHAR2(50),
    userStatus VARCHAR2(10),
    drinkSize VARCHAR2(50),
    coldDrink VARCHAR2(50),
    price DECIMAL(10,5),
    discount DECIMAL(10,5)
);
INSERT INTO Beverage
VALUES('Coke', 'Admin', 'Large', 'true', 2.50, 0);
INSERT INTO Beverage
VALUES('MochaLatte', 'User', 'Medium', 'false', 3.0, 0.1);
COMMIT;
