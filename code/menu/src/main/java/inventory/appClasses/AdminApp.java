/**
 * @author Desiree Mariano 2236344
 * @version November 2023
 * @description The AdminApp class represents the UI of the Admin when trying to access
 * the inventory from the Mcdonald class
 */

package inventory.appClasses;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

import inventory.menu.*;
import inventory.Colors;
import inventory.Coupon;
import inventory.Filter;
import inventory.Mcdonald;
import inventory.dataImports.ImportDataException;
import inventory.dataImports.McdoDatabase;
import inventory.interfaces.*;

public class AdminApp {
    public static void main (String [] args) throws IOException, SQLException, ImportDataException{
        Scanner scan = new Scanner(System.in);
        Filter filter = new Filter();
        Colors color = new Colors();
        
        // Connection to the database
        System.out.println(color.CYAN_UNDERLINED + "Please connect to the Database first! " + color.RESET);
        System.out.print("Enter your username: ");
        String username = scan.nextLine();
        System.out.print("Enter your password: ");
        String password = scan.nextLine();

       // Initiating the connection and retrieving the data from the database
        McdoDatabase mcdoDatabase = new McdoDatabase(username, password);
        // Set the data of burger and beverges.
        Burger.setMcdoDatabase(mcdoDatabase);   
        Beverages.setMcdoDatabase(mcdoDatabase);   

        List<IMenu> menu = mcdoDatabase.loadMenuFromDatabase();
        Mcdonald mcdonald = new Mcdonald(menu, filter, color);
        
        while (true){
            System.out.println(color.CYAN_UNDERLINED + "What do you want to do with the Mcdonald Inventory?: " + color.RESET);
            System.out.println("(1) View order as Admin");
            System.out.println("(2) Add Item to menu");
            System.out.println("(3) Remove an Item to menu");
            System.out.println("(4) View order as User");
            System.out.println("(5) Use a coupon?");
            System.out.println("(6) Exit");
            System.out.print(color.CYAN_BOLD + "Enter your answer here: " + color.RESET);
            int adminChoice = scan.nextInt();


            switch (adminChoice) {
                case 1:
                    // View order as Admin
                    System.out.println(color.GREEN_BOLD + "Viewing order as Admin..." + color.RESET);
                    mcdonald.displayAdminOrders();
                    break;
                case 2:
                    // Add Item to menu
                    System.out.println(color.CYAN_UNDERLINED + "What item do you want to add?" + color.RESET);
                    System.out.println("(1) Burger");
                    System.out.println("(2) Beverages");

                    System.out.print(color.CYAN_BOLD + "Enter your answer here: " + color.RESET);
                    int itemType = scan.nextInt();
                    scan.nextLine();

                    switch (itemType) {
                        case 1:
                            // Add Burger
                            System.out.println(color.CYAN_UNDERLINED + "Please enter burger order:" + color.RESET);
                            System.out.print("Enter the name of the product:");
                            String nameOfProduct = scan.nextLine();
                            System.out.print("Enter meat:");
                            String meat = scan.nextLine();
                            System.out.print("Enter veggies:");
                            String veggies = scan.nextLine();
                            System.out.print("Enter sauce:");
                            String sauce = scan.nextLine();

                            // Prompt for coupon details
                            System.out.print("Enter coupon discount:");
                            double discount = scan.nextDouble();
                            System.out.print("Enter price:");
                            double price = scan.nextDouble();

                            Coupon burgerCoupon = new Coupon();
                            burgerCoupon.setDiscount(discount);
                            String userStatusBurger = "Admin";

                            Burger burger = new Burger(nameOfProduct, userStatusBurger, meat, veggies, sauce, burgerCoupon.getDiscount(), price);
                            menu.add((IMenu) burger);
                            mcdonald.displayWholeMenu(menu);
                            break;
                            
        
                        case 2:
                            // Add a Beverage
                            System.out.println(color.CYAN_UNDERLINED + "Please enter beverage order:" + color.RESET);
                            System.out.print("Enter the name of the beverage:");
                            String nameOfProductBeverage = scan.nextLine();
                            System.out.print("Enter size:");
                            String size = scan.nextLine();
                            System.out.print("Enter the beverage if its cold or not (true/false):");
                            boolean coldDrink = scan.nextBoolean();

                            // Prompt for coupon details
                            System.out.print("Enter coupon discount:");
                            double beverageDiscount = scan.nextDouble();
                            System.out.print("Enter price:");
                            double beveragePrice = scan.nextDouble();

                            Coupon beverageCoupon = new Coupon();
                            beverageCoupon.setDiscount(beverageDiscount);
                            String userStatusBeverage = "Admin";
                            Beverages beverage = new Beverages(nameOfProductBeverage, userStatusBeverage, size, coldDrink, beveragePrice, beverageCoupon.getDiscount());
                            menu.add((IMenu) beverage);
                            mcdonald.displayWholeMenu(menu);
                            break;
        
                        default:
                            System.out.println(color.RED_BOLD + "Invalid choice" + color.RESET);
                            break;
                    }
                    break;
                case 3:
                    // Remove Item from menu
                    mcdonald.displayWholeMenu(menu);
                    System.out.println(color.CYAN_UNDERLINED + "Enter the index of the item to remove:" +  color.RESET);
                    int indexRemove = scan.nextInt();

                    mcdonald.removeItem(menu, indexRemove);
                    break;
                case 4:
                    // View order as User
                    System.out.println(color.GREEN_BOLD + "Viewing order as User..." + color.RESET);
                    mcdonald.displayUserOrders();
                    break;
                case 5:
                        mcdonald.displayWholeMenu(menu);

                        System.out.println(color.CYAN_UNDERLINED + "Enter the index of the item you want add the coupon to: " + color.RESET);

                        System.out.print("Enter discount:");
                        double discount = scan.nextDouble();
                        System.out.print("Enter price:");
                        double price = scan.nextDouble();
                        Coupon newCoupon = new Coupon();

                        double discountedPrice = newCoupon.applyCoupon(price, discount);
                        System.out.println("Discounted Price: " + discountedPrice);
                        break;
                case 6:
                    // Exit
                    System.out.println(color.GREEN_BOLD + "Exit complete! Goodbye!" + color.RESET);
                    System.exit(0);
                    break;
        
                default:
                    System.out.println(color.RED_BOLD + "Invalid choice" + color.RESET);
                    break;
            }
        }
    }
}
 