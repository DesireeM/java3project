/**
 * @author Sungeun Kim 2042714
 * @version November 2023
 * @description The UserApp class represents the UI of the User when trying to access
 * the inventory from the Mcdonald class
 */


package inventory.appClasses;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner; 
import inventory.menu.*;
import inventory.Colors;
import inventory.Coupon;
import inventory.Filter;
import inventory.Mcdonald;
import inventory.dataImports.FileOrderImporter;
import inventory.dataImports.ImportDataException;
import inventory.dataImports.McdoDatabase;
import inventory.interfaces.*;


public class UserApp {
    public static void main(String[] args) throws SQLException, ImportDataException {
        Scanner scan = new Scanner(System.in);
        Filter filter = new Filter();
        Colors color = new Colors();

        System.out.println(color.GREEN_UNDERLINED + "Please provide your username and password to connect to database." + color.RESET);
        String username = System.console().readLine("Username: ");
        String password = new String(System.console().readPassword("Password: "));

        // Initiating the connection and retrieving the data from the database
        McdoDatabase mcdoDatabase = new McdoDatabase(username, password);
        Burger.setMcdoDatabase(mcdoDatabase);   
        Beverages.setMcdoDatabase(mcdoDatabase);

        List<IMenu> menu = mcdoDatabase.loadMenuFromDatabase();
        Mcdonald mcdonald = new Mcdonald(menu, filter, color);        

        System.out.print(color.GREEN_UNDERLINED + "Your current status is: " + color.RESET);
        System.out.println("User");

        System.out.println("What do you want to do with the Mcdonald Inventory?: ");
        System.out.println("(1) View menu as Customer");
        System.out.println("(2) Order menu as Customer");
        System.out.println("(2) Use a coupon?");
        System.out.println("(3) Exit");
        System.out.println(color.GREEN_BOLD + "Enter your choice here: " + color.RESET);
        int customerChoice = scan.nextInt();

        switch (customerChoice) {
            case 1:
                // View all menu as Customer
                System.out.println(color.GREEN_UNDERLINED + "Viewing all the menu as customer..." + color.RESET);
                mcdonald.displayWholeMenu(menu);
                break;

            case 2:
                // Order menu as Customer
                System.out.println(color.GREEN_UNDERLINED + "What do you want to order from the menu?\nEnter name of the product: " + color.RESET);
                String orderProduct = scan.nextLine();
                mcdonald.displayUserOrderByName(orderProduct);
                break;

            case 3:
                // Usage of coupon --if yes, call the applyCoupon method to a specific menu, if no, call the applyNoCoupon
                mcdonald.displayWholeMenu(menu); 
                // Ask user to enter the index of the product
                System.out.println("Enter the index of the product you want to apply the coupon to:");
                int selectedIndex = scan.nextInt();

                // Get the selected item from the menu
                IMenu selectedItem = menu.get(selectedIndex);

                // Check if the selected item is a ChickenBurger
                if (selectedItem instanceof ChickenBurger) {
                    ChickenBurger selectedChickenBurger = (ChickenBurger) selectedItem;

                    System.out.println("Do you have a coupon for ChickenBurger? (yes/no)");
                    String couponResponse = scan.next();

                    if (couponResponse.equalsIgnoreCase("yes")) {
                        double discountedPrice = selectedChickenBurger.applyCoupon(selectedChickenBurger.getPrice(), selectedChickenBurger.getDiscount());
                        System.out.println("Final price after applying the coupon: $" + discountedPrice);
                    } else if (couponResponse.equalsIgnoreCase("no")) {
                        double discountedPrice = selectedChickenBurger.applyCoupon(selectedChickenBurger.getPrice(), 0);
                        System.out.println("Final price after applying the coupon: $" + discountedPrice);
                    } else {
                        System.out.println(color.RED_BOLD + "Invalid input entered please enter yes/no" + color.RESET);
                    }
                } else if (selectedItem instanceof VeggieBurger) {
                    VeggieBurger selectedVeggieBurger = (VeggieBurger) selectedItem;

                    System.out.println("Do you have a coupon for Veggie Burger? (yes/no)");
                    String couponResponse = scan.next();

                    if (couponResponse.equalsIgnoreCase("yes")) {
                        double discountedPrice = selectedVeggieBurger.applyCoupon(selectedVeggieBurger.getPrice(), selectedVeggieBurger.getDiscount());
                        System.out.println("Final price after applying the coupon: $" + discountedPrice);
                    } else if (couponResponse.equalsIgnoreCase("no")) {
                        double discountedPrice = selectedVeggieBurger.applyCoupon(selectedVeggieBurger.getPrice(), 0);
                        System.out.println("Final price after applying the coupon: $" + discountedPrice);
                    } else {
                        System.out.println(color.RED_BOLD + "Invalid input entered please enter yes/no" + color.RESET);
                    }
                } else if (selectedItem instanceof Coke) {
                    Coke selectedCoke = (Coke) selectedItem;

                    System.out.println("Do you have a coupon for Coke? (yes/no)");
                    String couponResponse = scan.next();

                    if (couponResponse.equalsIgnoreCase("yes")) {
                        double discountedPrice = selectedCoke.applyCoupon(selectedCoke.getPrice(), selectedCoke.getDiscount());
                        System.out.println("Final price after applying the coupon: $" + discountedPrice);
                    } else if (couponResponse.equalsIgnoreCase("no")) {
                        double discountedPrice = selectedCoke.applyCoupon(selectedCoke.getPrice(), 0);
                        System.out.println("Final price after applying the coupon: $" + discountedPrice);
                    } else {
                        System.out.println(color.RED_BOLD + "Invalid input entered please enter yes/no" + color.RESET);
                    }
                } else if (selectedItem instanceof MochaLatte) {
                    MochaLatte selectedMochaLatte = (MochaLatte) selectedItem;

                    System.out.println("Do you have a coupon for Mocha Latte? (yes/no)");
                    String couponResponse = scan.next();

                    if (couponResponse.equalsIgnoreCase("yes")) {
                        double discountedPrice = selectedMochaLatte.applyCoupon(selectedMochaLatte.getPrice(), selectedMochaLatte.getDiscount());
                        System.out.println("Final price after applying the coupon: $" + discountedPrice);
                    } else if (couponResponse.equalsIgnoreCase("no")) {
                        double discountedPrice = selectedMochaLatte.applyCoupon(selectedMochaLatte.getPrice(), 0);
                        System.out.println("Final price after applying the coupon: $" + discountedPrice);
                    } else {
                        System.out.println(color.RED_BOLD + "Invalid input entered please enter yes/no" + color.RESET);
                    }
                } else {
                    System.out.println(color.RED_BOLD + "Invalid input" + color.RESET);
                }
                break;
                
            case 4:
                System.out.println("Exiting the Admin App. Goodbye!");
                System.exit(0);
                break;
            default:
                System.out.println(color.RED_BOLD + "Invalid choice" + color.RESET);
                break;
        }

    }
}
