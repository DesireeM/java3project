package inventory;

import java.util.List;

import inventory.interfaces.*;
import inventory.menu.*;

/**
 * @description The Filter class implements the IFilter interface and provides methods to filter and display orders based on user status
 */
public class Filter implements IFilter{

    
    /**
     * @description Displays orders for users based on their user status
     * @param menuItems A list of menu items to filter based on status
     * @return A string containing details of orders for users
     */
    @Override
    public String DisplayOrderUser(List<IMenu> menuItems) {
        StringBuilder display = new StringBuilder();

        for (IMenu item : menuItems) {
            if (item instanceof Burger) {
                Burger burger = (Burger) item;
                // Check if the user status is "User"
                if ("User".equals(burger.getUserStatus())) {
                    // Append the details to the display string
                    display.append(burger.toString()); 
                    display.append("\n");
                }
            } else if (item instanceof Beverages) {
                Beverages beverages = (Beverages) item;
                // Check if the user status is "User"
                if ("User".equals(beverages.getUserStatus())) {
                    // Append the details to the display string
                    display.append(beverages.toString());
                    display.append("\n");
                }
            }
        }

        return display.toString();
    }

    
    /**
     * @description Displays orders for admins based on user status
     * @param menuItems A list of menu items to filter based on status
     * @return A string containing details of orders for admins
     */
    @Override
    public String DisplayOrderAdmin(List<IMenu> menuItems) {
        StringBuilder display = new StringBuilder();

        for (IMenu item : menuItems) {
            if (item instanceof Burger) {
                Burger burger = (Burger) item;
                // Check if the user status is "Admin"
                if ("Admin".equals(burger.getUserStatus())) {
                    // Append the details to the display string
                    display.append(burger.toString()); 
                    display.append("\n");
                }
            } else if (item instanceof Beverages) {
                Beverages beverages = (Beverages) item;
                // Check if the user status is "Admin"
                if ("Admin".equals(beverages.getUserStatus())) {
                    // Append the details to the display string
                    display.append(beverages.toString());
                    display.append("\n");
                }
            }
        }
        return display.toString();
    }

    /**
     * Displays a specific user order based on the name of the product.
     *
     * @param menuItems   A list of menu items to search for the specific product.
     * @param productName The name of the product to display.
     * @return A string containing details of the specific user order.
     */
    public String displayUserOrderByName(List<IMenu> menuItems, String productName) {
        StringBuilder display = new StringBuilder();

        for (IMenu item : menuItems) {
            if (item instanceof Burger) {
                Burger burger = (Burger) item;
                if (burger.getNameOfProduct().equals(productName)) {
                    display.append(burger.toString());
                    display.append("\n");
                }
            } else if (item instanceof Beverages) {
                Beverages beverages = (Beverages) item;
                if (beverages.getNameOfProduct().equals(productName)) {
                    display.append(beverages.toString());
                    display.append("\n");
                }
            }
        }

        if (display.length() == 0) {
            display.append("No orders found for ").append(productName);
        }
        return display.toString();
    }
}