package inventory.dataImports;

/**
 * @author Desiree Mariano 2236344
 * @version November 2023
 * @description The ImportDataException class is a custom exception that happens if the import
 * of data, such as loading menu information from CSV files doesnt work
 * @param cause The cause of the exception
 */
public class ImportDataException extends Exception {
    public ImportDataException(Throwable cause) {
        super(cause);
    }
}
