package inventory.dataImports;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import inventory.interfaces.IMenu;
import inventory.menu.Beverages;
import inventory.menu.Burger;


/**
 * @author Desiree Mariano 2236344
 * @version November 2023
 * @description The McdoDatabase class is a class that will make the connection with a database
 * and will add the tables created in the database
 */

public class McdoDatabase implements IMenu{
    private Connection conn;

     /**
     * @description Constructs a McdoDatabase object with the specific database connection information.
     * @param user     The username for connecting to the database
     * @param password The password for connecting to the database
     * @throws SQLException If a database error occurs
     */
    public McdoDatabase(String user, String password) throws SQLException {
        String url = "jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";
        this.conn = DriverManager.getConnection(url, user, password);
    }

    /**
     * @description Closes the database connection
     * @throws SQLException If a database access error occurs
     */
    public void Close() throws SQLException{
        if(conn != null && !conn.isClosed()){
            conn.close();
            conn = null;
        }
    }

     /**
     * @description Adds a Burger data to the database
     * @param nameOfProduct The name of the burger product
     * @param userStatus    The status of the user (admin or user)
     * @param meat          The type of meat in the burger
     * @param veggies       The vegetables in the burger
     * @param sauce         The sauce in the burger
     * @param discount      The discount value on the burger
     * @param price         The price of the burger
     * @throws SQLException If a database access error occurs
     */
    public void AddBurger(String nameOfProduct, String userStatus, String meat, String veggies, String sauce, double discount, double price) throws SQLException{
        Burger burger = new Burger(nameOfProduct, userStatus, meat, veggies, sauce, discount, price);
        burger.AddToDatabase(conn);
    }

      /**
     * @description Adds a Beverages data to the database
     * @param nameOfProduct The name of the beverage product
     * @param userStatus    The status of the user (admin or user)
     * @param size          The size of the beverage
     * @param coldDrink     Indicates whether the beverage is cold
     * @param price         The price of the beverage
     * @param discount      The discount value on the beverage
     * @throws SQLException If a database access error occurs
     */
    public void AddBeverage(String nameOfProduct, String userStatus, String size, boolean coldDrink, double price, double discount) throws SQLException{
        Beverages beverage = new Beverages(nameOfProduct, userStatus, size, coldDrink, price, discount);
        beverage.AddToDatabase(conn);
    }

    /**
     * @description Loads menu items from the database for both Burger and Beverages
     * @return List of IMenu representing the loaded menu items
     * @throws ImportDataException If there is an error importing data from the database
     * @throws SQLException        If a database error occurs
     */
    public List<IMenu> loadMenuFromDatabase() throws ImportDataException, SQLException {
        List<IMenu> menuItems = new ArrayList<>();

        menuItems.addAll(loadMenu("Burger"));
        menuItems.addAll(loadMenu("Beverage"));

        return menuItems;
    }

    
    /**
     * @description Loads menu items from the specified table in the database
     * @param tableName The name of the table to load menu items from
     * @return List of IMenu representing the loaded menu items
     * @throws ImportDataException If there is an error importing data from the database
     * @throws SQLException        If a database error occurs
     */
    public List<IMenu> loadMenu(String tableName) throws ImportDataException {
        List<IMenu> menuItems = new ArrayList<>();
        String sql = "SELECT * FROM " + tableName;
    
        try (PreparedStatement preparedStatement = conn.prepareStatement(sql);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                IMenu menuItem = createMenuItem(tableName, resultSet);
                menuItems.add(menuItem);
            }
        } catch (SQLException e) {
            throw new ImportDataException(e);
        }
    
        return menuItems;
    }

     /**
     * @description Creates an IMenu object based on the provided table name and result set
     * @param tableName The name of the table for which to create the menu item
     * @param resultSet The result set containing the data
     * @return An IMenu object representing the menu item
     * @throws SQLException If a database access error occurs
     */
    public IMenu createMenuItem(String tableName, ResultSet resultSet) throws SQLException {
        switch (tableName) {
            case "Burger":
                Burger burger = new Burger(
                        resultSet.getString("nameOfProduct"),
                        resultSet.getString("userStatus"),
                        resultSet.getString("meat"),
                        resultSet.getString("veggies"),
                        resultSet.getString("sauce"),
                        resultSet.getDouble("discount"),
                        resultSet.getDouble("price")
                );
                return (IMenu) burger;
            case "Beverages":
                Beverages beverage = new Beverages(
                    resultSet.getString("nameOfProduct"),
                    resultSet.getString("userStatus"),
                    resultSet.getString("size"),
                    resultSet.getBoolean("coldDrink"),
                    resultSet.getDouble("price"),
                    resultSet.getDouble("discount")
                );
                return (IMenu) beverage;
            default:
                throw new IllegalArgumentException("Invalid table name: " + tableName);
        }
    }
}
