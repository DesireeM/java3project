package inventory.dataImports;
import java.nio.file.*;
import java.io.IOException;
import java.util.*;

import inventory.Coupon;
import inventory.interfaces.*;
import inventory.menu.*;

/**
 * @author Desiree Mariano 2236344
 * @version November 2023
 * @description The FileOrderImporter class implements the IMenu interface and imports menu data from CSV files
 */
public class FileOrderImporter implements IMenu{
    private String burgerInfo;
    private String beveragesInfo;
    private List<IMenu> menuItems;


    /**
     *
     * @description Constructs a new FileOrderImporter object
     * @throws ImportDataException If there is an error loading menu data it will throw ImportDataException
     */
    public FileOrderImporter() throws ImportDataException, IOException {
        this.burgerInfo = "../../../../files/BurgerInfo.csv";
        this.beveragesInfo = "../../../../files/BeveragesInfo.csv";
        // declare list to store the data in
        this.menuItems = new ArrayList<>(); 
        try {
            menuItems.addAll(loadMenu(burgerInfo));
            menuItems.addAll(loadMenu(beveragesInfo));
        } catch (ImportDataException e) {
            throw new ImportDataException(e);
        }
    }

     /**
     * @description Loads menu data from a CSV file.
     *
     * @param filePath The file path of the CSV file.
     * @return List <IMenu> menuItems, items representing the menu.
     * @throws ImportDataException If there is an error loading menu data it will throw ImportDataException
     */
    public List<IMenu> loadMenu(String filePath) throws ImportDataException {
        List<IMenu> menuItems = new ArrayList<>();
    
        try {
            Path p = Paths.get(filePath);
            List<String> allLines = Files.readAllLines(p);

            for (String line : allLines) {
                String[] data = line.split(",");
                if ("Burger".equals(data[0])) {
                    Coupon coupon = new Coupon(); 
                    coupon.setDiscount(Double.parseDouble(data[6]));

                    menuItems.add((IMenu) new Burger(data[1], data[2], data[3], data[4], data[5], coupon.getDiscount(), Double.parseDouble(data[7]))) ;
                } else if ("Beverages".equals(data[0])) {
                    Coupon coupon = new Coupon(); 
                    coupon.setDiscount(Double.parseDouble(data[6]));
        
                    menuItems.add((IMenu) new Beverages(data[1], data[2], data[3], Boolean.parseBoolean(data[5]), coupon.getDiscount(), Double.parseDouble(data[7]))) ;               
                }
            }
        } catch (IOException | ArrayIndexOutOfBoundsException | NumberFormatException e) {
            throw new ImportDataException(e);
        }
    
        return menuItems;
    }
    
    /**
     * @description gets the file path of burger
     */
    public String getBurgerInfo() {
        return burgerInfo;
    }

      /**
     * @description gets the file path of beverages
     */
    public String getBeveragesInfo() {
        return beveragesInfo;
    }

    /**
     * @description Gets the list of menu items.
     * @return List<IMenu> menuItems, items in the menu.
     */
    public List<IMenu> getMenuItems() {
        return menuItems;
    }

}
