/**
 * @author Desiree Mariano 2236344
 * @version November 2023
 * @description The ChickenBurger class represents a chicken burger, extending the Burger class.
 * It initializes the properties of the chicken burger, such as the meat type, veggies, sauce, and coupon.
 */

 package inventory.menu;

public class ChickenBurger extends Burger {
    /**
     * @description Constructs a new ChickenBurger Object with the specified parameters.
     *
     * @param meat    The type of meat in the chicken burger.
     * @param veggies The types of veggies in the chicken burger.
     * @param sauce   The sauce used in the chicken burger.
     * @param coupon  The coupon value for potential discounts on the chicken burger.
     * @param price   The price value that the coupon will be applied to for the chicken burger.
     * 
     */
    public ChickenBurger(String nameOfProduct, String userStatus, String meat, String veggies, String sauce, double coupon, double price){
        super(nameOfProduct, userStatus, meat,veggies, sauce, coupon, price);
    }
}
