/**
 * @author Sungeun Kim 2042714
 * @version 11/17 2023
 * @description The VeggieBurger class represents a Veggie burger, extending the Burger class.
 * It initializes the properties of the veggie burger, such as the meat type, veggies, sauce, and coupon.
 */

package inventory.menu;

public class VeggieBurger extends Burger{

    /**
     * @description Constructs a new VeggieBurger object with the specified parameters.
     *
     * @param meat    The type of meat in the veggie burger.
     * @param veggies The types of veggies in the veggie burger.
     * @param sauce   The sauce used in the veggie burger.
     * @param price   The price of veggie burger.
     * @param coupon  The coupon value for potential discounts on the veggie burger.
     */
    public VeggieBurger(String nameOfProduct, String userStatus, String meat, String veggies, String sauce, double coupon, double price){
        super(nameOfProduct, userStatus, meat,veggies, sauce, coupon, price);
    }
}
