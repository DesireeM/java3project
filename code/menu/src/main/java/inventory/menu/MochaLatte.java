/**
 * @author Desiree Mariano 2236344
 * @version November 2023
 * @description The MochaLatte class represents a mocha latte beverage, extending the Beverages class
 * It initializes the properties of the mocha latte, such as the name, size, cold drink status, and coupon
 */

package inventory.menu;

/**
    * @description Constructs a new MochaLatte Object with the specified parameters
    * @param nameOfProduct The name of the mocha latte product
    * @param size          The size of the mocha latte
    * @param coldDrink     true, if the mocha latte is a cold drink; otherwise, its false
    * @param coupon        The coupon value for potential discounts on the mocha latte
    * @param price         The price value that the coupon will be applied to for the chicken burger.
    */
public class MochaLatte extends Beverages {
    public MochaLatte(String nameOfProduct, String userStatus, String size, boolean coldDrink, double price, double discount){
        super(nameOfProduct, userStatus, size, coldDrink, price, discount);
    }
}
