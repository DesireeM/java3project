/**
 * @author Sungeun Kim 2042714
 * @version 11/17 2023
 * @description The Coke class represents a coke beverage, extending the Beverages class
 * It initializes the properties of the coke, such as the name, size, cold drink status, and coupon
 */

package inventory.menu;

public class Coke extends Beverages {
    
    /**
     * @description Constructs a new Coke object with the specified parameters.
     *
     * @param nameOfProduct The name of the Coke product.
     * @param size          The size of the Coke.
     * @param coldDrink     Indicates whether the Coke is a cold drink.
     * @param price         price of Coke.
     * @param coupon        The coupon value for potential discounts on the Coke.
     */
    public Coke(String nameOfProduct, String userStatus, String size, boolean coldDrink, double price, double discount){
        super(nameOfProduct, userStatus, size, coldDrink, price, discount);
    }
}
