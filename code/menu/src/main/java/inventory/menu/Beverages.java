/**
 * @author Sungeun Kim 2042714
 * @version 11/17 2023
 * @description Beverages class represents a product with properties such as
 * size, cold drink or not, juice or soft drink, and a coupon for discounts.
*/

package inventory.menu;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import inventory.dataImports.ImportDataException;
import inventory.dataImports.McdoDatabase;
import inventory.interfaces.IBeverages;
import inventory.interfaces.IMenu;

public class Beverages implements IBeverages, IMenu {
    private String nameOfProduct;
    private String userStatus;
    private String size;
    private boolean coldDrink;
    private double price;
    private double discount;
    private static McdoDatabase mcdoDatabase;

    /**
     * @description Constructs a new Beverages object with the specified parameters.
     *
     * @param nameOfProduct The name of the beverage product.
     * @param userStatus The status of the User whether they are admin or user
     * @param size          The size of the beverage.
     * @param coldDrink     Indicates whether the beverage is cold.
     * @param price         price of Beverages.
     * @param discount      The discount value on the beverage.
     */
    public Beverages(String nameOfProduct, String userStatus, String size, boolean coldDrink, double price, double discount){
        this.nameOfProduct = nameOfProduct;
        this.userStatus = userStatus;
        this.size = size;
        this.coldDrink = coldDrink;
        this.price = price;
        this.discount = discount;
    }

    
    /**
     * @description Gets the name of the beverage product.
     * @return The name of the beverage product.
     */
    public String getNameOfProduct() {
        return this.nameOfProduct;
    }

    /**
     * @description Gets the status of the user.
     * @return The status of the user.
     */
    public String getUserStatus() {
        return this.userStatus;
    }

    /**
     * @description Gets the size of the beverages
     * @return The size of the beverage.
     */
    public String getSize() {
        return this.size;
    }

    /**
     * @description Checks if the beverage is a cold drink.
     * @return True if the beverage is a cold drink; otherwise, false.
     */
    public boolean getColdDrink() {
        return this.coldDrink;
    }

    /**
     * @description Gets the price of the beverage.
     * @return The of the beverage.
     */
    public double getPrice(){
        return this.price;
    }

    /**
     * @description Gets the coupon value for potential discounts on the beverage.
     * @return The coupon value for potential discounts on the beverage.
     */
    public double getDiscount(){
        return this.discount;
    }

    /**
     * @description Sets the name of the beverage product.
     * @param nameOfProduct  The name of the beverage product.
     */
    public void setNameOfProduct(String nameOfProduct) {
        this.nameOfProduct = nameOfProduct;
    }

    /**
     * @description sets the status of the user
     */
    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    /**
     * @description Sets the size of the beverages.
     * @param size  The size of the beverage product.
     */
    public void setSize(String size) {
        this.size = size;
    }

    /**
     * @description Sets the cold drink status
     * @param coldDrink  The cold drink status of beverage product.
     */
    public void setColdDrink(boolean coldDrink) {
        this.coldDrink = coldDrink;
    }

    /**
     * @description Sets the price of the beverages.
     * @param price  The price of beverage product.
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * @description Sets the coupon discount on the beverages.
     * @param coupon  The coupon discount on beverage product.
     */
    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public static void setMcdoDatabase(McdoDatabase database) {
        mcdoDatabase = database;
    }

    /**
     * @description Applies a coupon to the given price and returns the discounted price.
     *
     * @param price  The original price of the burger.
     * @param coupon The coupon value for potential discounts on the burger.
     * @return The discounted price after applying the coupon.
     */
    public double applyCoupon(double price, double discount){
        if (discount ==0)
        {
            return price;
        }
        
        return price - (price * discount);
    }

    /**
     * @description Checks if the beverage is cold.
     *
     * @param coldDrink Indicates whether the beverage is cold.
     * @return True if the beverage is cold; otherwise, false.
     */
    public boolean isCold(boolean coldDrink) {
        return coldDrink;
    }

    /**
     * @description Checks if the beverage is a soft drink.
     * it returns true if the beverage is not a juice.
     * @return True if the beverage is a soft drink; otherwise, false.
     */
    public boolean isSoftDrink() {
        if (isJuice() == false) {
            if ("Coke".equals(nameOfProduct)) {
                return true;
            }
            else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @description Checks if the beverage is a juice.
     * it returns true if the name of the product is "Coke" or "MochaLatte."
     * @return True if the beverage is a juice; otherwise, false.
     */
    public boolean isJuice() {
        if (("Coke".equals(nameOfProduct)) || ("Mocha Latte".equals(nameOfProduct))) {
            return false;
        }
        else {
            return true;
        }
    }

    public void AddToDatabase(Connection conn) throws SQLException{
        String statement = "insert into Beverage values (?,?,?,?,?,?,?)";
        try(PreparedStatement prepStmnt = conn.prepareStatement(statement)){
            prepStmnt.setString(0, getNameOfProduct());
            prepStmnt.setString(1, getUserStatus());
            prepStmnt.setString(2, getSize());
            prepStmnt.setBoolean(3, getColdDrink());
            prepStmnt.setDouble(5, getPrice());
            prepStmnt.setDouble(6, getDiscount());
            prepStmnt.executeUpdate();
        }
    }

    @Override
    public String toString() {
        return "Beverage: " + this.nameOfProduct + ", UserStatus: " + this.userStatus
                + ", Size: " + this.size + ", Cold drink or not: " + this.coldDrink
                + ", Price: " + this.price + ", Discount: " + this.discount;
    }

    @Override
    public List<IMenu> loadMenu(String tableName) throws ImportDataException, SQLException {
        // Check if McdoDatabase instance is set
        if (mcdoDatabase == null) {
            throw new IllegalStateException("McdoDatabase instance is not set. Call setMcdoDatabase before loading menu.");
        }

        // Delegating the loading to McdoDatabase
        return mcdoDatabase.loadMenu("Beverages");
    }
}