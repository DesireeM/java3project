/**
 * @author Desiree Mariano 2236344
 * @version November 2023
 * @description the class represents a generic burger item with properties such as
 * meat type, veggies, sauce, and a coupon for discounts.
 */

package inventory.menu;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import inventory.dataImports.ImportDataException;
import inventory.dataImports.McdoDatabase;
import inventory.interfaces.IBurger;
import inventory.interfaces.IMenu;


public class Burger implements IBurger, IMenu { 
    private String nameOfProduct;
    private String userStatus;
    private String meat;
    private String veggies;
    private String sauce;
    private double discount;
    private double price;
    private static McdoDatabase mcdoDatabase;



     /**
     * @description Constructs a new Burger Object with the specified parameters
     * meat, veggies, sauce, and coupon.
     *
     * @param nameOfProduct The type of the Product
     * @param userStatus The status of the User whether they are admin or user
     * @param meat    The type of meat in the burger.
     * @param veggies The types of veggies in the burger.
     * @param sauce   The sauce used in the burger.
     * @param coupon  The coupon value for potential discounts on the burger.
     * @param price   The price value that the coupon will be applied to for the chicken burger.
     */
    public Burger(String nameOfProduct, String userStatus,  String meat, String veggies, String sauce, double discount, double price){
        this.nameOfProduct = nameOfProduct;
        this.userStatus = userStatus;
        this.meat = meat;
        this.veggies = veggies;
        this.sauce = sauce;
        this.discount = discount;
        this.price = price;
    }

    /**
     * @description Gets the name of the burger.
     * @return The name of the burger.
     */
    public String getNameOfProduct() {
        return this.nameOfProduct;
    }

    /**
     * @description Gets the status of the user.
     * @return The status of the user.
     */
    public String getUserStatus() {
        return this.userStatus;
    }

    /**
     * @description Gets the type of meat in the burger.
     * @return The type of meat in the burger.
     */
    public String getMeat() {
        return this.meat;
    }

     /**
     * @description Gets the types of veggies in the burger.
     * @return The types of veggies in the burger.
     */
    public String getVeggies() {
        return this.veggies;
    }

    /**
     * @description Gets the sauce used in the burger.
     * @return The sauce used in the burger.
     */
    public String getSauce() {
        return this.sauce;
    }

    /**
     * @description Gets the coupon value for potential discounts on the burger.
     * @return The coupon value for potential discounts on the burger.
     */
    public double getDiscount(){
        return this.discount;
    }

    /**
     * @description Gets the price value that the coupon will be applied to for the burger.
     * @return The price value for potential discounts on the burger.
     */
    public double getPrice() {
        return this.price;
    }


     /**
     * @description Sets the name of the burger
     */
    public void setNameOfProduct(String nameOfProduct) {
        this.nameOfProduct = nameOfProduct;
    }
      /**
     * @description sets the status of the user
     */
    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }
    /**
     * @description Sets the type of meat in the burger.
     */
    public void setMeat(String meat) {
        this.meat = meat;
    }

      /**
     * @description Sets the types of veggies in the burger.
     */
    public void setVeggies(String veggies) {
        this.veggies = veggies;
    }

    
    /**
     * @description Sets the sauce used in the burger.
     */
    public void setSauce(String sauce) {
        this.sauce = sauce;
    }

     /**
     * @description Sets the coupon value for potential discounts on the burger.
     */
    public void setDiscount(double discount) {
        this.discount = discount;
    }

     /**
     * @description Sets the price value that the coupon will be applied to for the burger.
     */
    public void setPrice(double price) {
        this.price = price;
    }

    public static void setMcdoDatabase(McdoDatabase database) {
        mcdoDatabase = database;
    }
    
    /**
     * @description Applies a coupon to the given price and returns the discounted price.
     *
     * @param price  The original price of the burger.
     * @param coupon The coupon value for potential discounts on the burger.
     * @return The discounted price after applying the coupon.
     */
    public double applyCoupon(double price, double discount){
        if (discount == 0)
        {
            return price;
        }
        return price - (price*discount);
    }
    
      /**
     * @description Checks if the burger is vegetarian. In this implementation, it checks if the meat type
     * is "VegetarianBeef".
     * @return True if the burger is vegetarian; otherwise, false.
     */
    public boolean isVegetarian() {
        String meatType = getMeat();

        if ("chicken".equals(meatType) || "beef".equals(meatType)
            || "fish".equals(meatType) || "pork".equals(meatType)) {
            throw new IllegalArgumentException("Non-vegetarian meat type: " + meatType);
        }
        return "VegetarianBeef".equals(meatType);
    }    

    public void AddToDatabase(Connection conn) throws SQLException{
        String statement = "insert into Burger values (?,?,?,?,?,?,?)";
        try(PreparedStatement prepStmnt = conn.prepareStatement(statement)){
            prepStmnt.setString(0, getNameOfProduct());
            prepStmnt.setString(1, getUserStatus());
            prepStmnt.setString(2, getMeat());
            prepStmnt.setString(3, getVeggies());
            prepStmnt.setString(4, getSauce());
            prepStmnt.setDouble(5, getDiscount());
            prepStmnt.setDouble(6, getPrice());
            prepStmnt.executeUpdate();
        }
    }

    
    @Override
    public String toString() {
        return "Burger: " + this.nameOfProduct + ", UserStatus: " + this.userStatus
                + ", Meat: " + this.meat + ", Veggies: " + this.veggies
                + ", Sauce: " + this.sauce + ", Discount: " + this.discount + ", Price: " + this.price;
    }

    @Override
    public List<IMenu> loadMenu(String tableName) throws ImportDataException, SQLException {
        // Check if McdoDatabase instance is set
        if (mcdoDatabase == null) {
            throw new IllegalStateException("McdoDatabase instance is not set. Call setMcdoDatabase before loading menu.");
        }

        // Delegating the loading to McdoDatabase
        return mcdoDatabase.loadMenu("Burger");
    }


}