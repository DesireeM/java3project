
package inventory;

public class Coupon {
    private double discount;
    private double price;
    
    /**
     * @description Applies a coupon to the given price and returns the discounted price.
     * @param price  The original price of the item.
     * @param coupon The coupon value for potential discounts on the item.
     * @return The discounted price after applying the coupon.
     */

    public double applyCoupon(double price, double discount){
        if (discount == 0)
        {
            return price;
        }
        return price - (price*discount);
    }

    /**
     * @description Gets the discount value of the coupon.
     * @return The discount value.
     */
    public double getDiscount() {
        return discount;
    }

    /**
     * @description Gets the price of the coupon.
     * @return The price value.
     */
    public double getPrice() {
        return price;
    }

     /**
     * @description Sets the discount value of the coupon.
     * @param discount The new discount value to set.
     */
    public void setDiscount(double discount) {
        this.discount = discount;
    }

     /**
     * @description Sets the price of the coupon.
     * @param price The new price to set.
     */
    public void setPrice(double price) {
        this.price = price;
    }

     /**
     * @description Updates the discount and price values of the coupon.
     * @param newDiscount The new discount value to set.
     * @param newPrice    The new price value to set.
     */
    public void updateCoupon(double newDiscount, double newPrice) {
        this.discount = newDiscount;
        this.price = newPrice;
    }

}