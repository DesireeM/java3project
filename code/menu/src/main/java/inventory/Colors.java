package inventory;
/**
 * @author Desiree Mariano 2236344
 * @version November 2023
 * @description The Colors class is a class to add color to the console, so you can call the fields in this class
 */

public class Colors {
    public String RESET = "\033[0m";  // Text Reset
    // Bold
    public String RED_BOLD = "\033[1;31m";    // RED
    public String GREEN_BOLD = "\033[1;32m";  // GREEN
    public String CYAN_BOLD = "\033[1;36m";   // CYAN

    // Underline
    public String RED_UNDERLINED = "\033[4;31m";    // RED
    public String GREEN_UNDERLINED = "\033[4;32m";  // GREEN
    public String CYAN_UNDERLINED = "\033[4;36m";   // CYAN

}
