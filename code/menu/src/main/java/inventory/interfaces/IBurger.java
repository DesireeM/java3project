package inventory.interfaces;
/**
 * @author Desiree Mariano 2236344
 * @version November 2023
 * @description The IBurger interface is the blueprint of all methods for the Burger class
 */

public interface IBurger {
    String getNameOfProduct();
    String getUserStatus();
    String getMeat();
    String getVeggies();
    String getSauce();
    void setNameOfProduct(String nameOfProduct);
    void setUserStatus(String userStatus);
    void setMeat(String meat);
    void setVeggies(String veggies);
    void setSauce(String sauce);
    boolean isVegetarian();
    
    double applyCoupon(double price, double discount);
    double getPrice();
    double getDiscount();
    void setDiscount(double discount);
    void setPrice(double price);

    String toString();
}
