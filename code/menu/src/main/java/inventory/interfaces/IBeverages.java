package inventory.interfaces;

public interface IBeverages {
    String getNameOfProduct();
    String getSize();
    boolean getColdDrink();
    void setNameOfProduct(String nameOfProduct);
    void setSize(String size);
    void setColdDrink(boolean coldDrink);
    boolean isCold(boolean coldDrink);
    boolean isSoftDrink();
    boolean isJuice();

    double applyCoupon(double price, double discount);
    double getPrice();
    double getDiscount();
    void setDiscount(double discount);
    void setPrice(double price);

    String toString();

}
