package inventory.interfaces;
import java.sql.SQLException;
import java.util.List;
import inventory.dataImports.ImportDataException;

/**
 * @description The IMenu interface defines a method for loading menu data from a file.
 */
public interface IMenu {

/**
 * @description Loads menu data from the Mcdonald.sql database .
 * @param tableName The table name of the associated type of food.
 * @return A list of IMenu items representing the menu.
 * @throws ImportDataException If there is an error loading menu data.
 * @throws SQLException if there is an error loading menu data
 */
 List <IMenu> loadMenu(String tableName) throws ImportDataException, SQLException;


}
