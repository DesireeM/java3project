package inventory.interfaces;

import java.util.List;

/**
 * @description The IFilter interface defines methods for displaying order details based on
 * user roles, such as user and admin
 */
public interface IFilter {
    /**
     * @description Display data order of  user
     */
    String DisplayOrderUser(List<IMenu> menuItems);

      /**
     * @description Display data order for admin
     */
    String DisplayOrderAdmin(List<IMenu> menuItems);

    String displayUserOrderByName(List<IMenu> menuItems, String productName);
}
