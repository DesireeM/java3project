package inventory;
import java.util.*;

import inventory.interfaces.*;


/**
 * @author Desiree Mariano 2236344
 * @version November 2023
 * @description The Mcdonald class represents a McDonald's restaurant with a menu and methods
 * for managing and displaying orders
 */
public class Mcdonald{
    private List<IMenu> menu;
    private IFilter filter;
    private Colors color;

    /**
 * @description Constructs a McDonald's restaurant with the specified menu and filter
 * @param menu   The menu of the McDonald's restaurant
 * @param filter The filter used for displaying orders
 */
    public Mcdonald(List<IMenu> menu, IFilter filter, Colors color) {
        this.menu = menu;
        this.filter = filter;
        this.color = color; // Fixed the assignment
    }

    /**
     * Removes an item from the menu based on the user's input index
     * 
     * @param menuItems   The list of menu items to remove from
     * @param indexRemove The index of the item to remove
     */
    public void removeItem(List<IMenu> menuItems, int indexRemove) {
        if (indexRemove >= 1 && indexRemove <= menuItems.size()) {
            int adjustedIndex = indexRemove - 1;
            System.out.println(color.RED_BOLD + "Item removed: " + menuItems.get(adjustedIndex) + color.RESET );
            menuItems.remove(adjustedIndex);
            displayWholeMenu(menuItems);
        } else {
            System.out.println(color.RED_BOLD + "Invalid index. No item removed." + color.RESET);
        }
    }

    /**
     * Displays user orders for all items in the menu.
     */
    public void displayUserOrders() {
        String userOrders = filter.DisplayOrderUser(menu);
        System.out.println("User Orders: " + userOrders);
    }

    /**
     * Displays admin orders for all items in the menu.
     */
    public void displayAdminOrders() {
        String adminOrders = filter.DisplayOrderAdmin(menu);
        System.out.println("Admin Orders: " + adminOrders);
    }

  /**
     * Displays the entire menu including the newly added items from the current menu and the database menu
     *
     * @param databaseMenu The menu loaded from the database
     */
    public void displayWholeMenu(List<IMenu> menuItems) {
        menu = menuItems;

        System.out.println(color.GREEN_UNDERLINED + "Menu:" + color.RESET);
        for (int i = 1; i <= menu.size(); i++) {
            System.out.println("Index " + i + ": " + menu.get(i - 1));
        }
    }
    /**
     * Displays a specific user order based on the name of the product.
     *
     * @param productName The name of the product to display.
     */
    public void displayUserOrderByName(String productName) {
        String userOrder = filter.displayUserOrderByName(menu, productName);
        System.out.println(userOrder);
    }
   
}