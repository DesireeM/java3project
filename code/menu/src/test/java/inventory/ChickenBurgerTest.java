/**
 * @author Desiree Mariano 2236344
 * @version November 2023
 * @description  The ChickenBurgerTest class contains unit tests for the ChickenBurger class.
 */

package inventory;
import static org.junit.Assert.*;
import org.junit.Test;
import inventory.menu.ChickenBurger;

public class ChickenBurgerTest {

    /**
     * @description Tests the getNameOfProduct() method of the ChickenBurger class.
     */
    @Test
    public void testGetNameOfProduct() {
        ChickenBurger chickenBurger = new ChickenBurger("ChickenBurger", "User", "Chicken", "Lettuce", "Mayo", 0.25, 6);
        assertEquals("ChickenBurger", chickenBurger.getNameOfProduct());
    }

    /**
     * @description Tests for the getUserStatus method of ChickenBurger class.
     */
    @Test
    public void testGetUserStatus() {
        ChickenBurger chickenBurger = new ChickenBurger("ChickenBurger", "User", "Chicken", "Lettuce", "Mayo", 0.25, 6);
        assertEquals("User", chickenBurger.getUserStatus());
    }

     /**
     * @description Tests the getMeat() method of the ChickenBurger class.
     */
    @Test
    public void testGetMeat() {
        ChickenBurger chickenBurger = new ChickenBurger("ChickenBurger", "User", "Chicken", "Lettuce", "Mayo", 0.25, 6);
        assertEquals("Chicken", chickenBurger.getMeat());
    }

     /**
     * @description Tests the getVeggies() method of the ChickenBurger class.
     */
    @Test
    public void testGetVeggies() {
        ChickenBurger chickenBurger = new ChickenBurger("ChickenBurger", "User", "Chicken", "Lettuce", "Mayo", 0.25, 6);
        assertEquals("Lettuce", chickenBurger.getVeggies());
    }

     /**
     * @description Tests the getSauce() method of the ChickenBurger class.
     */
    @Test
    public void testGetSauce() {
        ChickenBurger chickenBurger = new ChickenBurger("ChickenBurger", "User", "Chicken", "Lettuce", "Mayo", 0.25, 6);
        assertEquals("Mayo", chickenBurger.getSauce());
    }

    
     /**
     * @description Tests the getDiscount() method of the ChickenBurger class.
     */
    @Test
    public void testGetDiscount() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0.25);
        double actualDiscount = coupon.getDiscount();
        assertEquals(0.25, actualDiscount, 0.001); 
    }

      /**
     * @description Tests the getPrice() method of the ChickenBurger class.
     */
    @Test
    public void testGetPrice() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0.25);
        ChickenBurger chickenBurger = new ChickenBurger("ChickenBurger", "User", "Chicken", "Lettuce", "Mayo", coupon.getDiscount(), 6);
        assertEquals(6, chickenBurger.getPrice(), 0.001);
    }

    /**
     * @description Tests the setNameOfProduct() method of the ChickenBurger class.
     */
    @Test
    public void testSetNameOfProduct() {
        ChickenBurger chickenBurger = new ChickenBurger("ChickenBurger", "User", "Chicken", "Lettuce", "Mayo", 0.25, 6);
        chickenBurger.setNameOfProduct("SpicyChickenBurger");
        assertEquals("SpicyChickenBurger", chickenBurger.getNameOfProduct());
    }

    /**
     * @description Tests for the setUserStatus method of the ChickenBurger class.
     */
    @Test
    public void testSetUserStatus() {
        ChickenBurger chickenBurger = new ChickenBurger("ChickenBurger", "User", "Chicken", "Lettuce", "Mayo", 0.25, 6);
        chickenBurger.setUserStatus("Admin");
        assertEquals("Admin", chickenBurger.getUserStatus());
    }

    /**
     * @description Tests the setMeat() method of the ChickenBurger class.
     */
    @Test
    public void testSetMeat() {
        ChickenBurger chickenBurger = new ChickenBurger("ChickenBurger", "User", "Chicken", "Lettuce", "Mayo", 0.25, 6);
        chickenBurger.setMeat("Turkey");
        assertEquals("Turkey", chickenBurger.getMeat());
    }

    /**
     * @description Tests the setVeggies() method of the ChickenBurger class.
     */
    @Test
    public void testSetVeggies() {
        ChickenBurger chickenBurger = new ChickenBurger("ChickenBurger", "User", "Chicken", "Lettuce", "Mayo", 0.25, 6);
        chickenBurger.setVeggies("Tomato");
        assertEquals("Tomato", chickenBurger.getVeggies());
    }

    /**
     * @description Tests the setSauce() method of the ChickenBurger class.
     */
    @Test
    public void testSetSauce() {
        ChickenBurger chickenBurger = new ChickenBurger("ChickenBurger", "User", "Chicken", "Lettuce", "Mayo", 0.25, 6);
        chickenBurger.setSauce("SpicyMayo");
        assertEquals("SpicyMayo", chickenBurger.getSauce());
    }

    /**
     * @description Tests the setDiscount() method of the ChickenBurger class.
     */
    @Test
    public void testSetDiscount() {
        ChickenBurger chickenBurger = new ChickenBurger("ChickenBurger", "User", "Chicken", "Lettuce", "Mayo", 0.25, 6);
        chickenBurger.setDiscount(0.2);
        assertEquals(0.2, chickenBurger.getDiscount(), 0.001);
    }

    /**
     * @description Tests the setPrice() method of the ChickenBurger class.
     */
    @Test
    public void testSetPrice() {
        ChickenBurger chickenBurger = new ChickenBurger("ChickenBurger", "User", "Chicken", "Lettuce", "Mayo", 0.25, 6);
        chickenBurger.setPrice(8);
        assertEquals(8, chickenBurger.getPrice(), 0.001);
    }


    /**
     * @description Tests the applyCoupon() method of the ChickenBurger class.
     */
    @Test
    public void testApplyCoupon() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0.25);
        ChickenBurger chickenBurger = new ChickenBurger("ChickenBurger", "User", "Chicken", "Lettuce", "Mayo", coupon.getDiscount(), 6);
        double discountedPrice = chickenBurger.applyCoupon(6, coupon.getDiscount());
        assertEquals(6 - (6 * 0.25), discountedPrice, 0.001);
    }
    
     /**
     * @description Tests the applyCoupon() method of the ChickenBurger class with zero coupon.
     */
    @Test
    public void testApplyCouponWithZeroCoupon() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0);
        ChickenBurger chickenBurger = new ChickenBurger("ChickenBurger", "User", "Chicken", "Lettuce", "Mayo", coupon.getDiscount(), 6);
        double discountedPrice = chickenBurger.applyCoupon(6, 0);
        assertEquals(6, discountedPrice, 0.001); 
    }

    /**
     * @description Tests the isVegetarian() method of the ChickenBurger class for a non-vegetarian burger.
     */
    @Test
    public void testIsVegetarian() {
        ChickenBurger chickenBurger = new ChickenBurger("ChickenBurger", "User", "Chicken", "Lettuce", "Mayo", 0.25, 6);
        assertFalse(chickenBurger.isVegetarian());
    }

    //! test method for addToDatabase

    /**
     * @description Test for toString() method of the Burger class.
     */
    @Test
    public void testToString() {
        ChickenBurger chickenBurger = new ChickenBurger("ChickenBurger", "User", "Chicken", "Lettuce", "Mayo", 0.25, 6.0);
        assertEquals("Burger: ChickenBurger, UserStatus: User, Meat: Chicken, Veggies: Lettuce, Sauce: Mayo, Discount: 0.25, Price: 6.0", chickenBurger.toString());
    }
}