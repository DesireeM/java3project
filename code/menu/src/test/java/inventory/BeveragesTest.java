/**
 * @author Sungeun Kim 2042714
 * @version 11/17 2023
 * @description The BeveragesTest class contains unit tests for the Beverages class.
 */

package inventory;
import static org.junit.Assert.*;
import org.junit.Test;
import inventory.menu.Beverages;


public class BeveragesTest {
    /**
     * @description Tests the getNameOfProduct method of Beverages class.
     */
    @Test
    public void testGetNameOfProduct() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0.1);
        Beverages beverage = new Beverages("Cola", "User", "Medium", true, 2.50, 0);
        assertEquals("Cola", beverage.getNameOfProduct());
    }

    /**
     * @description Tests the getUserStatus method of Beverages class.
     */
    @Test
    public void testGetUserStatus() {
        Beverages beverage = new Beverages("Cola", "User", "Medium", true, 2.50, 0);
        assertEquals("User", beverage.getUserStatus());
    }

    /**
     * @description Tests the getSize method of Beverages class.
     */
    @Test
    public void testGetSize() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0.5);
        Beverages beverage = new Beverages("Steamed Milk", "User", "Medium", false, 0.05, 0);
        assertEquals("Medium", beverage.getSize());
    }

    /**
     * @description Tests the getColdDrink method of Beverages class.
     */
    @Test
    public void testGetColdDrink() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0.1);
        Beverages beverage = new Beverages("Cola", "Admin", "Medium", true, 2.50, coupon.getDiscount());
        assertTrue(beverage.getColdDrink());
    }

    /**
     * @description Test for the getPrice method.
     */
    @Test
    public void testGetPrice() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0);
        Beverages beverage = new Beverages("Cola", "Admin", "Medium", true, 2.50, coupon.getDiscount());
        assertEquals(2.50, beverage.getPrice(), 0.001);
    }

    /**
     * @description Tests the getDiscount method of Beverages class.
     */
    @Test
    public void testGetDiscount() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0.10);
        double actualDiscount = coupon.getDiscount();
        assertEquals(0.10, actualDiscount, 0.001); 
    }

    /**
     * @description Tests for the setNameOfProduct method.
     */
    @Test
    public void testSetNameOfProduct() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0.10);
        Beverages beverage = new Beverages("Cola", "User", "Medium", true, 2.50, 1);
        beverage.setNameOfProduct("Pepsi");
        assertEquals("Pepsi", beverage.getNameOfProduct());
    }

    /**
     * @description Tests for the setUserStatus method.
     */
    @Test
    public void testSetUserStatus() {
        Beverages beverage = new Beverages("Cola", "User", "Medium", true, 2.50, 1.25);
        beverage.setUserStatus("Admin");
        assertEquals("Admin", beverage.getUserStatus());
    }

    /**
     * @description Tests for the setSize method.
     */
    @Test
    public void testSetSize() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0.7);
        Beverages beverage = new Beverages("Cola", "Admin", "Medium", true, 2.50, 1.0);
        beverage.setSize("Large");
        assertEquals("Large", beverage.getSize());
    }

    /**
     * @description Tests for the setColdDrink method.
     */
    @Test
    public void testSetColdDrink() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0.7);
        Beverages beverage = new Beverages("Cola", "Admin", "Medium", true, 2.50, 3.0);
        beverage.setColdDrink(false);
        assertFalse(beverage.getColdDrink());
    }

    /**
     * @description Tests for the setPrice method.
     */
    @Test
    public void testSetPrice() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0.7);
        Beverages beverage = new Beverages("Cola", "Admin", "Medium", true, 2.50, coupon.getDiscount());
        beverage.setPrice(3.0);
        assertEquals(3.0, beverage.getPrice(), 0.01);
    }

    /**
     * @description Tests for the setDiscount method.
     */
    @Test
    public void testSetDiscount() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0.7);
        Beverages beverage = new Beverages("Cola", "Admin", "Medium", true, 2.50, coupon.getDiscount());
        assertEquals(0.7, beverage.getDiscount(), 0.001);
    }

    /**
     * @description Test for the applyCoupon method with no discount.
     */
    @Test
    public void testApplyCouponWithNoDiscount() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0);
        Beverages beverage = new Beverages("Cola", "Admin", "Medium", true, 2.50, coupon.getDiscount());
        double discountedPrice = beverage.applyCoupon(2.50, 0);
        assertEquals(2.50, discountedPrice, 0.001);
    }

    /**
     * @description Test for the applyCoupon method with discount.
     */
    @Test
    public void testApplyCouponWithDiscount() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0.5);
        Beverages beverage = new Beverages("Cola", "Admin", "Medium", true, 2.50, coupon.getDiscount());
        double discountedPrice = beverage.applyCoupon(2.50, 0.5);
        assertEquals(1.25, discountedPrice, 0.001);
    }

    /**
     * @description Test for the isCold method when the beverage is cold.
     */
    @Test
    public void testIsColdTrue() {

        Beverages beverage = new Beverages("Coke", "Admin", "Large", true, 2.00, 0);
        assertTrue(beverage.isCold(beverage.getColdDrink()));
    }

    /**
     * @description Test for the isCold method when the beverage is not cold.
     */
    @Test
    public void itestIColdFalse() {
        Beverages beverage = new Beverages("hot tea", "Admin", "Large", false, 2.00, 0);
        assertFalse(beverage.isCold(beverage.getColdDrink()));
    }

    /**
     * @description Test for the isSoftDrink method when the beverage is a soft drink.
     */
    @Test
    public void testIsSoftDrinkTrue() {
        Beverages beverage = new Beverages("Coke", "Admin", "Large", true, 2.00, 0);
        assertTrue(beverage.isSoftDrink());
    }

    /**
     * @description Test for the isSoftDrink method when the beverage is not a soft drink.
     */
    @Test
    public void testIsSoftDrinkFalse() {
        Beverages beverage = new Beverages("Latte", "Admin", "Large", true, 2.00, 0);
        assertFalse(beverage.isSoftDrink());
    }

    /**
     * @description Test for the isJuice method when the beverage is a juice.
     */
    @Test
    public void testIsJuiceTrue() {
        Beverages beverage = new Beverages("Orange juice", "Admin", "Large", true, 2.00, 0);
        assertTrue(beverage.isJuice());
    }

    /**
     * @description Test for the isJuice method when the beverage is not a juice.
     */
    @Test
    public void testIsJuiceFalse() {
        Beverages beverage = new Beverages("Coke", "Admin", "Large", true, 2.00, 0);
        assertFalse(beverage.isJuice());
    }

    //! test method for addToDatabase

    /**
     * @description Test for toString() method of the Beverages class.
     */
    @Test
    public void testToString() {
        Beverages beverage = new Beverages("IcedTea", "User", "Medium", true, 1.99, 0.05);
        assertEquals("Beverage: IcedTea, UserStatus: User, Size: Medium, Cold drink or not: true, Price: 1.99, Discount: 0.05", beverage.toString());
    }
}
