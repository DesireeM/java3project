/**
 * @author Sungeun Kim 2042714
 * @version 11/17 2023
 * @description The VeggieBurgerTest class contains unit tests for the VeggieBurger class.
 */

package inventory;
import static org.junit.Assert.*;
import org.junit.Test;
import inventory.menu.VeggieBurger;

public class VeggieBurgerTest {

    /**
     * @description Tests the getNameOfProduct() method of the VeggieBurger class.
     */
    @Test
    public void testGetNameOfProduct() {
        VeggieBurger veggieBurger = new VeggieBurger("Veggie burger", "Admin", "VegetarianBeef", "Lettuce, Tomato", "Ketchup", 0.15, 8);
        assertEquals("Veggie burger", veggieBurger.getNameOfProduct());
    }

    /**
     * @description Tests for the getUserStatus method of VeggieBurger class.
     */
    @Test
    public void testGetUserStatus() {
        VeggieBurger veggieBurger = new VeggieBurger("Veggie burger", "Admin", "VegetarianBeef", "Lettuce, Tomato", "Ketchup", 0.15, 8);
        assertEquals("Admin", veggieBurger.getUserStatus());
    }

    /**
     * @description Tests the getMeat() method of the VeggieBurger class.
     */
    @Test
    public void getMeat() {
        VeggieBurger veggieBurger = new VeggieBurger("Veggie burger", "Admin", "VegetarianBeef", "Lettuce, Tomato", "Ketchup", 0.15, 8);
        assertEquals("Veggie burger", veggieBurger.getNameOfProduct());
    }

     /**
     * @description Tests the getVeggies() method of the VeggieBurger class.
     */
    @Test
    public void testGetVeggies() {
        VeggieBurger veggieBurger = new VeggieBurger("Veggie burger", "Admin", "VegetarianBeef", "Lettuce, Tomato", "Ketchup", 0.15, 8);
        assertEquals("Lettuce, Tomato", veggieBurger.getVeggies());
    }

    /**
     * @description Tests the getSauce() method of the VeggieBurger class.
     */
    @Test
    public void testGetSauce() {
        VeggieBurger veggieBurger = new VeggieBurger("Veggie burger", "Admin", "VegetarianBeef", "Lettuce, Tomato", "Ketchup", 0.15, 8);
        assertEquals("Ketchup", veggieBurger.getSauce());
    }

    /**
     * @description Tests the getDiscount() method of the VeggieBurger class.
     */
    @Test
    public void testGetCoupon() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0.15);
        VeggieBurger veggieBurger = new VeggieBurger("Veggie burger", "Admin", "VegetarianBeef", "Lettuce, Tomato", "Ketchup", coupon.getDiscount(), 8);
        assertEquals(coupon.getDiscount(), veggieBurger.getDiscount(), 0.001);
    }

    /**
     * @description Tests the setNameOfProduct() method of the VeggieBurger class.
     */
    @Test
    public void testSetNameOfProduct() {
        VeggieBurger veggieBurger = new VeggieBurger("Veggie burger", "Admin", "VegetarianBeef", "Lettuce, Tomato", "Ketchup", 0.15, 8);
        veggieBurger.setNameOfProduct("Spicy Veggie burger");
        assertEquals("Spicy Veggie burger", veggieBurger.getNameOfProduct());
    }

    /**
     * @description Tests for the setUserStatus method of the VeggieBurger class.
     */
    @Test
    public void testSetUserStatus() {
        VeggieBurger veggieBurger = new VeggieBurger("Veggie burger", "Admin", "VegetarianBeef", "Lettuce, Tomato", "Ketchup", 0.15, 8);
        veggieBurger.setUserStatus("User");
        assertEquals("User", veggieBurger.getUserStatus());
    }

     /**
     * @description Test for the setMeat method of the VeggieBurger class.
     */
    @Test
    public void testSetMeat() {
        VeggieBurger veggieBurger = new VeggieBurger("Veggie burger", "Admin", "VegetarianBeef", "Lettuce, Tomato", "Ketchup", 0.15, 8);
        veggieBurger.setMeat("BeyondMeat");
        assertEquals("BeyondMeat", veggieBurger.getMeat());
    }

    /**
     * @description Test for the setVeggies method of the VeggieBurger class.
     */
    @Test
    public void testSetVeggies() {
        VeggieBurger veggieBurger = new VeggieBurger("Veggie burger", "Admin", "VegetarianBeef", "Lettuce, Tomato", "Ketchup", 0.15, 8);
        veggieBurger.setVeggies("Spinach, Onion");
        assertEquals("Spinach, Onion", veggieBurger.getVeggies());
    }

    /**
     * @descriptionTest for the setSauce method of the VeggieBurger class.
     */
    @Test
    public void testSetSauce() {
        VeggieBurger veggieBurger = new VeggieBurger("Veggie burger", "Admin", "VegetarianBeef", "Lettuce, Tomato", "Ketchup", 0.15, 8);
        veggieBurger.setSauce("Mustard");
        assertEquals("Mustard", veggieBurger.getSauce());
    }

    /**
     * @description Test for the setDiscount method of the VeggieBurger class.
     */
    @Test
    public void testSetDiscount() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0.15);
        double actualDiscount = coupon.getDiscount();
        assertEquals(0.15, actualDiscount, 0.001); 
    }

    /**
     * @description Test for the setPrice method of the VeggieBurger class.
     */
    @Test
    public void testSetPrice() {

        VeggieBurger veggieBurger = new VeggieBurger("Veggie burger", "Admin", "VegetarianBeef", "Lettuce, Tomato", "Ketchup", 0.15, 8);
        veggieBurger.setPrice(6.00);
        assertEquals(6.00, veggieBurger.getPrice(), 0.001);
    }
    

    
    /**
     * @description Tests the applyCoupon() method of the VeggieBurger class.
     */
    @Test
    public void testApplyCoupon() {
        VeggieBurger veggieBurger = new VeggieBurger("Veggie burger", "Admin", "VegetarianBeef", "Lettuce, Tomato", "Ketchup", 0.15, 8);
        double discountedPrice = veggieBurger.applyCoupon(8,0.15);
        assertEquals(6.80, discountedPrice, 0.001);
    }

    /**
     * @description Tests the applyCoupon() method of the VeggieBurger class with zero coupon.
     */
    @Test
    public void testApplyCouponWithZeroCoupon() {
        VeggieBurger veggieBurger = new VeggieBurger("Veggie burger", "Admin", "VegetarianBeef", "Lettuce, Tomato", "Ketchup", 0, 8);
        double discountedPrice = veggieBurger.applyCoupon(8, 0);
        assertEquals(8, discountedPrice, 0.001); 
    }

    /**
     * @desctiption Tests the isVegetarian method of VeggieBurger class.
     * A VeggieBurger is considered vegetarian if its meat type is "VegetarianBeef."
     */
    @Test
    public void testIsVegetarian() {
        VeggieBurger veggieBurger = new VeggieBurger("Veggie burger", "Admin", "VegetarianBeef", "Lettuce, Tomato", "Ketchup", 0.15, 8);
        assertTrue(veggieBurger.isVegetarian());
    }

    //! test method for addToDatabase

    /**
     * @description Test for toString() method of the Burger class.
     */
    @Test
    public void testToString() {
        VeggieBurger veggieBurger = new VeggieBurger("Veggie burger", "Admin", "VegetarianBeef", "Lettuce, Tomato", "Ketchup", 0.15, 8);
        assertEquals("Burger: Veggie burger, UserStatus: Admin, Meat: VegetarianBeef, Veggies: Lettuce, Tomato, Sauce: Ketchup, Discount: 0.15, Price: 8.0", veggieBurger.toString());
    }
}
