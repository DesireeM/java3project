/**
 * @author Desiree Mariano 2236344
 * @version November 2023
 * @description The BurgerTest class contains unit tests for the Burger class.
 */
package inventory;
import static org.junit.Assert.*;
import org.junit.Test;

import inventory.menu.Burger;

public class BurgerTest {

    /**
     * @description Tests the getNameOfProduct() method of the Burger class.
     */
    @Test
    public void testGetNameOfProduct() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0.15);
        Burger burger = new Burger("Burger", "User", "Beef", "Lettuce", "Ketchup", coupon.getDiscount(), 8);
        assertEquals("Burger", burger.getNameOfProduct());
    }

    /**
     * @description Tests for the getUserStatus method of Burger class.
     */
    @Test
    public void testGetUserStatus() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0.15);
        Burger burger = new Burger("Beef Burger", "Admin", "Beef", "Onion, Pickles", "Ketchup", coupon.getDiscount(), 9);
        assertEquals("Admin", burger.getUserStatus());
    }

    /**
     * @description Tests the getMeat() method of the Burger class.
     */
    @Test
    public void testGetMeat() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0.15);
        Burger burger = new Burger("Cheeseburger", "Admin","Beef", "tomato", "Ketchup", coupon.getDiscount(), 8);
        assertEquals("Beef", burger.getMeat());
    }

     /**
     * @description Tests the getVeggies() method of the Burger class.
     */
    @Test
    public void testGetVeggies() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0.15);
        Burger burger = new Burger("Hamburger", "User", "Beef", "tomato", "Ketchup", coupon.getDiscount(), 8);
        assertEquals("tomato", burger.getVeggies());
    }

     /**
     * @description Tests the getSauce() method of the Burger class.
     */
    @Test
    public void testGetSauce() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0.15);
        Burger burger = new Burger("Cheeseburger", "Admin", "Beef", "Lettuce", "Ketchup", coupon.getDiscount(), 8);
        assertEquals("Ketchup", burger.getSauce());
    }

     /**
     * @description Tests the getDiscount() method of the Burger class.
     */
    @Test
    public void testGetDiscount() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0.15);
        double actualDiscount = coupon.getDiscount();
        assertEquals(0.15, actualDiscount, 0.001); 
    }

      /**
     * @description Tests the getPrice() method of the Burger class.
     */
    @Test
    public void testGetPrice() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0.15);
        Burger burger = new Burger("Burger","User", "Beef", "Lettuce", "Ketchup", coupon.getDiscount(), 8);
        assertEquals(8, burger.getPrice(), 0.001);
    }


    /**
     * @description Tests the setNameOfProduct() method of the Burger class.
     */
    @Test
    public void testSetNameOfProduct() {
        Burger burger = new Burger("Burger", "Admin", "Beef", "Lettuce", "Ketchup", 0, 8);
        burger.setNameOfProduct("VeggieBurger");
        assertEquals("VeggieBurger", burger.getNameOfProduct());
    }

    /**
     * @description Tests for the setUserStatus method of the Burger class.
     */
    @Test
    public void testSetUserStatus() {
        Burger burger = new Burger("Beef Burger", "Admin", "Beef", "Onion, Pickles", "Ketchup", 0.15, 9);
        burger.setUserStatus("User");
        assertEquals("User", burger.getUserStatus());
    }

    /**
     * @description Tests the setMeat() method of the Burger class.
     */
    @Test
    public void testSetMeat() {
        Burger burger = new Burger("Burger", "Admin", "Beef", "Lettuce", "Ketchup", 0, 8);
        burger.setMeat("Chicken");
        assertEquals("Chicken", burger.getMeat());
    }

    /**
     * @description Tests the setVeggies() method of the Burger class.
     */
    @Test
    public void testSetVeggies() {
        Burger burger = new Burger("Burger", "User", "Beef", "Lettuce", "Ketchup", 0, 8);
        burger.setVeggies("Tomato");
        assertEquals("Tomato", burger.getVeggies());
    }

      /**
     * @description Tests the setSauce() method of the Burger class.
     */
    @Test
    public void testSetSauce() {
        Burger burger = new Burger("Burger", "Admin", "Beef", "Lettuce", "Ketchup", 0, 8);
        burger.setSauce("Mayo");
        assertEquals("Mayo", burger.getSauce());
    }

    /**
     * @description Tests the setDiscount() method of the Burger class.
     */
    @Test
    public void testSetDiscount() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0.15);
        Burger burger = new Burger("Burger", "Admin", "Beef", "Lettuce", "Ketchup", coupon.getDiscount(), 8);
        burger.setDiscount(0.2);
        assertEquals(0.2, burger.getDiscount(), 0.001);
    }
    /**
     * @description Tests the setPrice() method of the Burger class.
     */
    @Test
    public void testSetPrice() {
        Burger burger = new Burger("Burger", "User", "Beef", "Lettuce", "Ketchup", 0, 8);
        burger.setPrice(10);
        assertEquals(10, burger.getPrice(), 0.001);
    }

     /**
     * @description Test the application of a coupon to the price of a Burger object with a Coupon.
     */
    @Test
    public void testApplyCoupon() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0.15);
        Burger burger = new Burger("Hamburger", "User", "Beef", "tomato", "Ketchup", coupon.getDiscount(), 8);
        double discountedPrice = burger.applyCoupon(8, coupon.getDiscount());

        assertEquals(8 - (8 * 0.15), discountedPrice, 0.001);
    }

    /**
     * @description Test the application of of the price of a Burger object without a discount.
     */
    @Test
    public void testApplyCouponWithNoDiscount() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0);
        Burger burger = new Burger("Burger", "Admin", "Beef", "Lettuce", "Ketchup", coupon.getDiscount(), 8);
        double discountedPrice = burger.applyCoupon(8, 0);

        assertEquals(8, discountedPrice, 0.001);
    }

        /**
     * @description Tests the isVegetarian() method of the Burger class for a vegetarian burger.
     */
    @Test
    public void testIsVegetarianForVegetarianBurger() {
        Burger burger = new Burger("VeggieBurger", "Admin", "VegetarianBeef", "Lettuce", "Ketchup", 0, 8);
        assertTrue(burger.isVegetarian());
    }

    /**
     * @description Tests the isVegetarian() method of the Burger class for a non-vegetarian burger.
     */
    @Test
    public void testIsVegetarianForNonVegetarianBurger() {
        Burger burger = new Burger("ChickenBurger", "User", "Chicken", "Lettuce", "Ketchup", 0, 8);
        assertFalse(burger.isVegetarian());
    }

    //! test method for addToDatabase

    /**
     * @description Test for toString() method of the Burger class.
     */
    @Test
    public void testToString() {
        Burger burger = new Burger("Classic Burger", "User", "Beef", "Lettuce, Tomato, Onion", "Ketchup", 0.1, 5.99);
        assertEquals("Burger: Classic Burger, UserStatus: User, Meat: Beef, Veggies: Lettuce, Tomato, Onion, Sauce: Ketchup, Discount: 0.1, Price: 5.99", burger.toString());
    }
}
