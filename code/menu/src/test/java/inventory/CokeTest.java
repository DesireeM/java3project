/**
 * @author Sungeun Kim 2042714
 * @version 11/17 2023
 * @description The CokeTest class contains unit tests for the Coke class.
 */

package inventory;
import static org.junit.Assert.*;
import org.junit.Test;
import inventory.menu.Coke;

public class CokeTest {
    
    /**
     * @description Test for the constructor of the Coke class.
     */
    @Test
    public void testConstructor() {

        Coupon coupon = new Coupon();
        coupon.setDiscount(0);
        Coke coke = new Coke("Coke", "Admin", "Large", true, 2.50, 0);

        assertEquals("Coke", coke.getNameOfProduct());
        assertEquals("Admin", coke.getUserStatus());
        assertEquals("Large", coke.getSize());
        assertTrue(coke.getColdDrink());
        assertEquals(2.50, coke.getPrice(), 0.001);
        assertEquals(coupon.getDiscount(), coke.getDiscount(), 0.001);
    }

    /**
     * @description Test for the getNameOfProduct method.
     */
    @Test
    public void testGetNameOfProduct() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0);
        Coke coke = new Coke("Coke", "Admin", "Large", true, 2.50, 0);
        assertEquals("Coke", coke.getNameOfProduct());
    }

    /**
     * @description Tests the getUserStatus method of Coke class.
     */
    @Test
    public void testGetUserStatus() {
        Coke coke = new Coke("Coke", "Admin", "Large", true, 2.50, 0);
        assertEquals("Admin", coke.getUserStatus());
    }

    /**
     * @description Test for the getSize method.
     */
    @Test
    public void testGetSize() {
        Coke coke = new Coke("Coke", "Admin", "Large", true, 2.50, 0);
        assertEquals("Large", coke.getSize());
    }

    /**
     * @description Test for the getColdDrink method.
     */
    @Test
    public void testGetColdDrink() {
        Coke coke = new Coke("Coke", "Admin", "Large", true, 2.50, 0);
        assertTrue(coke.getColdDrink());
    }

    /**
     * @description Test for the getPrice method.
     */
    @Test
    public void testGetPrice() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0);
        Coke coke = new Coke("Coke", "Admin", "Large", true, 2.50, coupon.getDiscount());
        assertEquals(2.50, coke.getPrice(), 0.001);
    }

    /**
     * @description Test for the getDiscount method.
     */
    @Test
    public void testGetDicsount() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0);
        double actualDiscount = coupon.getDiscount();
        assertEquals(0, actualDiscount, 0.001); 
    }

    /**
     * @description Test for the setNameOfProduct method.
     */
    @Test
    public void testSetNameOfProduct() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0);
        Coke coke = new Coke("Coke", "Admin", "Large", true, 2.50, 0);
        coke.setNameOfProduct("Diet Coke");
        assertEquals("Diet Coke", coke.getNameOfProduct());
    }

    /**
     * @description Test for the setSize method.
     */
    @Test
    public void testSetSize() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0);
        Coke coke = new Coke("Coke", "Admin", "Large", true, 2.50, 0);
        coke.setSize("Large");
        assertEquals("Large", coke.getSize());
    }

    /**
     * @description Test for the setColdDrink method.
     */
    @Test
    public void testSetColdDrink() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0);
        Coke coke = new Coke("Coke", "Admin", "Large", true, 2.50, 0);
        coke.setColdDrink(false);
        assertFalse(coke.getColdDrink());
    }

    /**
     * @description Test for the setPrice method.
     */
    @Test
    public void testSetPrice() {
        Coke coke = new Coke("Coke", "Admin", "Large", true, 2.50, 0);
        coke.setPrice(3.00);
        assertEquals(3.00, coke.getPrice(), 0.001);
    }

    /**
     * @description Test for the setCoupon method.
     */
    @Test
    public void testSetCoupon() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0.7);
        Coke coke = new Coke("Coke", "Admin", "Large", true, 2.50, coupon.getDiscount());
        assertEquals(coupon.getDiscount(), coke.getDiscount(), 0.001);
    }

    /**
     * @description Tests the isCold method of Coke class.
     */
    @Test
    public void testIsCold() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0);
        Coke coke = new Coke("Coke", "Admin", "Large", true, 2.50, 0);
        assertTrue(coke.isCold(coke.getColdDrink()));
    }

    /**
     * @description Tests the isSoftDrink method of Coke class.
     */
    @Test
    public void testIsSoftDrink() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0);
        Coke coke = new Coke("Coke", "Admin", "Large", true, 2.50, 0);
        assertTrue(coke.isSoftDrink());
    }

    /**
     * @description Test for the isJuice method when the Coke is not a juice.
     */
    @Test
    public void testIsJuice() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0);
        Coke coke = new Coke("Coke", "Admin", "Large", true, 2.50, 0);
        assertFalse(coke.isJuice());
    }

    /**
     * @description Test for the applyCoupon method when the Coke has no discount.
     */
    @Test
    public void testApplyCouponWithNoDiscount() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0);
        Coke coke = new Coke("Coke", "Admin", "Large", true, 2.50, coupon.getDiscount());
        double discountedPrice = coke.applyCoupon(2.50, 0);
        assertEquals(2.50, discountedPrice, 0.001);
    }

    /**
     * @description Test for the applyCoupon method when the Coke has a discount.
     */
    @Test
    public void testApplyCouponWithDiscount() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0.1);
        Coke coke = new Coke("Coke", "Admin", "Large", true, 2.50, coupon.getDiscount());
        double discountedPrice = coke.applyCoupon(2.50, 0.1);
        assertEquals(2.25, discountedPrice, 0.001);
    }

    //! test method for addToDatabase

    /**
     * @description Test for toString() method of the Coke class.
     */
    @Test
    public void testToString() {
        Coke coke = new Coke("Coke", "Admin", "Large", true, 2.5, 0.0);
        assertEquals("Beverage: Coke, UserStatus: Admin, Size: Large, Cold drink or not: true, Price: 2.5, Discount: 0.0", coke.toString());
    }
}
