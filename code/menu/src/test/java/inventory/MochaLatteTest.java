/**
 * @author Desiree Mariano 2236344
 * @version November 2023
 * @description The MochaLatteTest class contains unit tests for the MochaLatte class.
 */

package inventory;
import static org.junit.Assert.*;
import org.junit.Test;
import inventory.menu.MochaLatte;


public class MochaLatteTest {
     /**
     * Tests the getNameOfProduct() method of the MochaLatte class.
     */
    @Test
    public void testGetNameOfProduct() {
        MochaLatte mochaLatte = new MochaLatte("Mocha Latte", "User", "Medium", false, 3.0, 0.10);
        assertEquals("Mocha Latte", mochaLatte.getNameOfProduct());
    }

    /**
     * @description Tests the getUserStatus method of Beverages class.
     */
    @Test
    public void testGetUserStatus() {
        MochaLatte mochaLatte = new MochaLatte("Mocha Latte", "User", "Medium", false, 3.0, 0.10);
        assertEquals("User", mochaLatte.getUserStatus());
    }

     /**
     * Tests the getSize() method of the MochaLatte class.
     */
    @Test
    public void testGetSize() {
        MochaLatte mochaLatte = new MochaLatte("Mocha Latte", "User", "Medium", false, 3.0, 0.10);
        assertEquals("Medium", mochaLatte.getSize());
    }

      /**
     * Tests the getColdDrink() method of the MochaLatte class.
     */
    @Test
    public void testGetColdDrink() {
        MochaLatte mochaLatte = new MochaLatte("Mocha Latte", "User", "Medium", false, 3.0, 0.10);
        assertFalse(mochaLatte.getColdDrink());
    }

    /**
     * @description Test for the getPrice method.
     */
    @Test
    public void testGetPrice() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0);
        MochaLatte mochaLatte = new MochaLatte("Mocha Latte", "User", "Medium", false, 3.0, 0.10);
        assertEquals(3.0, mochaLatte.getPrice(), 0.001);
    }

    /**
     * Tests the getDiscount() method of the MochaLatte class.
     */
    @Test
    public void testGetDiscount() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0.10);
        double actualDiscount = coupon.getDiscount();
        assertEquals(0.10, actualDiscount, 0.001);
    }

    /**
     * @description Tests for the setNameOfProduct method.
     */
    @Test
    public void testSetNameOfProduct() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0.10);
        MochaLatte mochaLatte = new MochaLatte("Mocha Latte", "User", "Medium", false, 3.0, 0.10);
        mochaLatte.setNameOfProduct("Pepsi");
        assertEquals("Pepsi", mochaLatte.getNameOfProduct());
    }

    /**
     * @description Tests for the setUserStatus method.
     */
    @Test
    public void testSetUserStatus() {
        MochaLatte mochaLatte = new MochaLatte("Mocha Latte", "User", "Medium", false, 3.0, 0.10);
        mochaLatte.setUserStatus("Admin");
        assertEquals("Admin", mochaLatte.getUserStatus());
    }

    /**
     * @description Tests for the setSize method.
     */
    @Test
    public void testSetSize() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0.7);
        MochaLatte mochaLatte = new MochaLatte("Mocha Latte", "User", "Medium", false, 3.0, 0.10);
        mochaLatte.setSize("Large");
        assertEquals("Large", mochaLatte.getSize());
    }

    /**
     * @description Tests for the setColdDrink method.
     */
    @Test
    public void testSetColdDrink() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0.7);
        MochaLatte mochaLatte = new MochaLatte("Mocha Latte", "User", "Medium", false, 3.0, 0.10);
        mochaLatte.setColdDrink(true);
        assertTrue(mochaLatte.getColdDrink());
    }

    /**
     * @description Tests for the setPrice method.
     */
    @Test
    public void testSetPrice() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0.7);
        MochaLatte mochaLatte = new MochaLatte("Mocha Latte", "User", "Medium", false, 3.0, 0.10);
        mochaLatte.setPrice(4.0);
        assertEquals(4.0, mochaLatte.getPrice(), 0.001);
    }

    /**
     * @description Tests for the setDiscount method.
     */
    @Test
    public void testSetDiscount() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0.7);
        MochaLatte mochaLatte = new MochaLatte("Mocha Latte", "User", "Medium", false, 3.0, coupon.getDiscount());
        assertEquals(0.7, mochaLatte.getDiscount(), 0.001);
    }

    /**
     * Tests the applyCoupon() method of the MochaLatte class.
     */
    @Test
    public void testApplyCoupon() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0.1);
        MochaLatte mochaLatte = new MochaLatte("Mocha Latte", "User", "Medium", false, coupon.getDiscount(), 0.10);
        double discountedPrice = mochaLatte.applyCoupon(4, coupon.getDiscount());
        assertEquals(3.6, discountedPrice, 0.001);
    }

      /**
     * Tests the applyCoupon() method of the MochaLatte class with zero coupon.
     */
    @Test
    public void testApplyCouponWithZeroCoupon() {
        Coupon coupon = new Coupon();
        coupon.setDiscount(0);
        MochaLatte mochaLatte = new MochaLatte("Mocha Latte", "User", "Medium", false, 3.0, 0.10);
        double discountedPrice = mochaLatte.applyCoupon(4, 0.0);
        assertEquals(4, discountedPrice, 0.001);
    }

    /**
     * @description Test for the isCold method.
     */
    @Test
    public void itestICold() {
        MochaLatte mochaLatte = new MochaLatte("Mocha Latte", "User", "Medium", false, 3.0, 0.10);
        assertFalse(mochaLatte.isCold(mochaLatte.getColdDrink()));
    }

    /**
     * @description Test for the isSoftDrink method.
     */
    @Test
    public void testIsSoftDrink() {
        MochaLatte mochaLatte = new MochaLatte("Mocha Latte", "User", "Medium", false, 3.0, 0.10);
        assertFalse(mochaLatte.isSoftDrink());
    }

    /**
     * @description Test for the isJuice method.
     */
    @Test
    public void testIsJuice() {
        MochaLatte mochaLatte = new MochaLatte("Mocha Latte", "User", "Medium", false, 3.0, 0.10);
        assertFalse(mochaLatte.isJuice());
    }

    //! test method for addToDatabase

    /**
     * @description Test for toString() method of the MochaLatte class.
     */
    @Test
    public void testToString() {
        MochaLatte mochaLatte = new MochaLatte("Mocha Latte", "User", "Medium", false, 3.0, 0.1);
        assertEquals("Beverage: Mocha Latte, UserStatus: User, Size: Medium, Cold drink or not: false, Price: 3.0, Discount: 0.1", mochaLatte.toString());
    }
}
